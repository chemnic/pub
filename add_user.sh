#!/usr/bin/env bash

set -o errexit; set -o pipefail; set -o nounset
##set -o xtrace

_raise_error() { printf "%s\n" "ERROR: ${1}"; exit 1; }

[[ $# = 2 ]] || _raise_error "Wrong number of arguments supplied."

readonly i_target_user="${1}"
readonly i_public_key="${2}"

# shellcheck disable=SC1091
source /etc/os-release
case "${ID}" in
    ubuntu | debian)
        readonly family="debian"
        ;;
    sangoma | centos | nethserver | rocky)
        readonly family="fedora"
        ;;
    alpine)
        readonly family="alpine"
        readonly useradd_pkg="shadow"
        ;;
    ipfire)
        ;;
    *)
        _raise_error "Can not determine distribution."
        ;;
esac

_install() {
    local package="${1}"

    case "${family}" in
        debian)
            apt-get --quiet --quiet --no-install-recommends  install "${package}" > /dev/null
            ;;
        fedora)
            yum --quiet --assumeyes install "${package}"
            ;;
        alpine)
            apk --quiet add "${package}"
            ;;
        *)
            _raise_error "Error."
            ;;
    esac
}

_add_user() {
    local target_user="${1}"
    local randompw
    randompw="$(tr -dc A-Za-z0-9 </dev/urandom | head -c 20; exit 0)"

    [[ $(command -v useradd) ]] || _install "${useradd_pkg}"
    useradd --system --create-home --user-group --shell /bin/bash --home "/var/lib/${target_user}" "${target_user}"
    printf "%s" "${target_user}:${randompw}" | chpasswd
}

_add_pub_key() {
    local target_user="${1}"
    local public_key="${2}"
    local auth_file="/var/lib/${target_user}/.ssh/authorized_keys"

    [[ -d $(dirname "${auth_file}") ]] || mkdir "$(dirname "${auth_file}")"
    printf "%s\n" "ssh-rsa ${public_key}" > "${auth_file}"
    chown --recursive "${target_user}.${target_user}" "$(dirname "${auth_file}")"
    chmod 700 "$(dirname "${auth_file}")"
    chmod 600 "${auth_file}"
}

_temp_sudoers() {
    local target_user="${1}"
    local sudoers_file="/etc/sudoers.d/zz-manager-temp"

    printf "%s\n" "${target_user} ALL=(ALL) NOPASSWD:ALL" 2>&1 |
        EDITOR='tee' visudo --file="${sudoers_file}" > /dev/null 2>&1
}

_start_sshd() {
    case "${family}" in
        debian | fedora)
            systemctl start sshd
            systemctl enable sshd
            ;;
        alpine)
            rc-update --quiet add sshd default
            rc-service --quiet sshd start > /dev/null
            ;;
        *)
            _raise_error "Error."
            ;;
    esac
}

_main() {
    [[ $(command -v sudo) ]] || _install sudo
    [[ $(command -v sshd) ]] || _install openssh-server
    [[ $(pidof sshd) ]] || _start_sshd
    [[ $(id -u "${i_target_user}" 2>/dev/null) ]] || _add_user "${i_target_user}"
    _add_pub_key "${i_target_user}" "${i_public_key}"
    _temp_sudoers "${i_target_user}"
}

_main
